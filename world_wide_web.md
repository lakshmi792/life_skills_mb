# **World Wide Web**

## **Introduction :**
The World Wide Web is abbreviated as WWW and is commonly known as the web. WWW can be defined as the collection of different websites around the world, containing different information shared via local servers(or computers). Users can access the content of websites from any part of the world over the internet using their devices such as computers, laptops, cell phones, etc. The WWW, along with the internet, enables the retrieval and display of text and media to your device. A web page is given an online address called a Uniform Resource Locator (URL). A particular collection of web pages that belong to a specific URL is called a website, e.g., www.facebook.com, www.google.com, etc. So, the World Wide Web is like a huge electronic book whose pages are stored on multiple servers across the world.



### Web Browsers :

* A Web browser is used to access web pages. 
* Web browsers can be defined as programs which display text, data, pictures, animation and video on the Internet. 
* Hyperlinked resources on the World Wide Web can be accessed using software interfaces provided by Web browsers.

![World wide web](https://media.geeksforgeeks.org/wp-content/uploads/20190717221909/Untitled-Diagram-125.jpg)

## **History :**


* The World Wide Web was invented by a British scientist, Tim Berners-Lee in 1989. 
* He was working at CERN at that time. 
* Originally, it was developed by him to fulfill the need of automated information sharing between scientists across the world, so that they could easily share the data and results of their experiments and studies with each other.

### **Components of the Web:**

**There are 3 components of the web :**

1. Uniform Resource Locator (URL): serves as a system for resources on the web. 
2. HyperText Transfer Protocol (HTTP): specifies communication of browser and server. 
3. Hyper Text Markup Language (HTML): defines the structure, organisation and content of a webpage. 

![basic url structure](https://cdn.ttgtmedia.com/rms/onlineimages/networking-basic_url_structure.png)


# Client Server Architecture :

* The client-server architecture or model is an application network separating tasks between clients and servers that are either within the same system or need to communicate over a network. 
* In order to access the service provided by the server, the server-client sends the request to another program, which runs a few programs that distribute work among the clients & share resources with them.

**Working of Client-Server Model**

* **Client** : Client is a computer (Host) i.e. capable of receiving information or using a particular service from the service providers (Servers).
* **Servers** : Server is a remote computer which provides information (data) or access to particular services.

So, it is basically the Client requesting something and the Server serving it as long as it is present in the database.

![client-server model](https://media.geeksforgeeks.org/wp-content/uploads/20191016114416/801.png)

### **Browsers interaction with the server :**

There are few steps to follow to interacts with the servers a client.

* User enters the URL(Uniform Resource Locator) of the website or file. The Browser then requests the DNS(DOMAIN NAME SYSTEM) Server.
* DNS Server lookup for the address of the WEB Server.
* DNS Server responds with the IP address of the WEB Server.
* Browser sends over an HTTP/HTTPS request to WEB Server’s IP (provided by DNS server).
* Server sends over the necessary files of the website.
* Browser then renders the files and the website is displayed. This rendering is done with the help of DOM (Document Object Model) interpreter, CSS interpreter and JS Engine collectively known as the JIT or (Just in Time) Compilers.


![Browser interaction with the server](https://media.geeksforgeeks.org/wp-content/uploads/20191016120927/8110.png)

---

### There are different types of client server architecture :

**Tier 1 Architecture** :
one-tier architecture keeps all of the elements of an application, including the interface, middleware and back-end data, in one place.

![Tier 1 architecture](https://www.softwaretestingmaterial.com/wp-content/uploads/2016/06/one-tier-software-architecture.png)

---

**Tier 2 Architecture** :
In a two-tier architecture, the client is on the first tier. The database server and web application server reside on the same server machine, which is the second tier. This second tier serves the data and executes the business logic for the web application.

![Two-Tier Architecture](https://www.softwaretestingmaterial.com/wp-content/uploads/2016/06/two-tier-software-architecture.png)

---
 
**Tier 3 Architecture** :
Three-tier architecture, which separates applications into three logical and physical computing tiers, is the predominant software architecture for traditional client-server applications.

![Three-Tier Architecture](https://www.softwaretestingmaterial.com/wp-content/uploads/2016/06/three-tier-software-architecture.png)



## Hypertext Transfer Protocol (HTTP) :
---

1. Hyper Text Transfer Protocol (HTTP) is an application layer protocol which enables WWW to work smoothly and effectively. 

2. It is based on a client-server model. The client is a web browser which communicates with the web server which hosts the website. 

3. This protocol defines how messages are formatted and transmitted and what actions the Web Server and browser should take in response to different commands. 
4. When you enter a URL in the browser, an HTTP command is sent to the Web server, and it transmits the requested Web Page.

5. When we open a website using a browser, a connection to the web server is opened, and the browser communicates with the server through HTTP and sends a request. HTTP is carried over TCP/IP to communicate with the server. 

6. The server processes the browser's request and sends a response, and then the connection is closed. Thus, the browser retrieves content from the server for the user.

![HTTP](https://cdn.ttgtmedia.com/rms/onlineimages/whatis-how_http_works.png)


### **DNS :**

DNS (Domain Name System) allows you to interact with devices on the Internet without having to remember long strings of numbers. Each computer on the Internet has its own unique address, known as an IP address, just like every home has a unique address for sending direct mail.


* 104.26.10.228 is an IP address consisting of four sets of numbers extending from 0 to 255 separated by a period. 
* It’s not easy having to remember this complicated collection of numbers every time you want to access a website, which is where DNS comes in handy.


### **TCP :**
* TCP stands for Transmission Control Protocol
* TCP (Transmission Control Protocol) is one of the main protocols of the Internet protocol suite. 
* It lies between the Application and Network Layers which are used in providing reliable delivery services. 
* It is a connection-oriented protocol for communications that helps in the exchange of messages between the different devices over a network.

### **UDP :**
* User Datagram Protocol (UDP) is a communications protocol that is primarily used to establish low-latency and loss-tolerating connections between applications on the internet. 
* UDP speeds up transmissions by enabling the transfer of data before an agreement is provided by the receiving party.



### **References :**

- [https://www.geeksforgeeks.org/world-wide-web-www/](https://www.geeksforgeeks.org/world-wide-web-www/)
- [https://www.javatpoint.com/what-is-world-wide-web](https://www.javatpoint.com/what-is-world-wide-web)
- [https://www.geeksforgeeks.org/client-server-model/](https://www.geeksforgeeks.org/client-server-model/)
- [https://www.geeksforgeeks.org/details-on-dns/#:~:text=DNS%20(Domain%20Name%20System)%20allows,address%20for%20sending%20direct%20mail.](https://www.geeksforgeeks.org/details-on-dns/#:~:text=DNS%20(Domain%20Name%20System)%20allows,address%20for%20sending%20direct%20mail.)
- [https://www.interviewbit.com/blog/client-server-architecture/](https://www.interviewbit.com/blog/client-server-architecture/)
- [https://www.researchgate.net/profile/Trevor-Mudge/publication/221147997/figure/fig1/AS:339672459956226@1457995633431/A-Typical-3-Tier-Server-Architecture-Tier-1-Web-Server-Tier-2-Application-Server-Tier.png](https://www.researchgate.net/profile/Trevor-Mudge/publication/221147997/figure/fig1/AS:339672459956226@1457995633431/A-Typical-3-Tier-Server-Architecture-Tier-1-Web-Server-Tier-2-Application-Server-Tier.png)
- [https://www.softwaretestingmaterial.com/software-architecture/](https://www.softwaretestingmaterial.com/software-architecture/)














