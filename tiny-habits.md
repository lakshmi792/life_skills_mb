# Tiny Habits

## 1.Your takeaways from the video (Minimum 5 points)

* Forget big change, start with a tiny habit.
* Celebrate after getting a tiny habit.
* To develop a habit have a motivation.
* We can make drastic change with making minor tweeks in daily routine. 

## 2.Your takeaways from the video in as much detail as possible

* Celebrate after getting a tiny habit.
* Shrink the behaviour/Take the first step: Shrink the behaviour into tinier habit which  requires little motivation and has bigger impact.
* Find a behaviour that is easy to do in 30 seconds or less.
* Find the tiniest habit with the biggest impact.

## 3.How can you use B = MAP to make making new habits easier?

* Start with a small habit which requires low motivation to complete, and we have the high ability to do it. Have behaviours which trigger those habits.

## 4.Why it is important to "Shine" or Celebrate after each successful completion of habit?

* A pat on the back is always a good motivation. So,it is important to celebrate after each successful completion of habit.

## 5.Your takeaways from the video (Minimum 5 points)

There are four stages of Habit Formation.

* Noticing.
* Wanting.
* Doing.
* Liking.

## 6.Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

* With outcome-based habits, the focus is on what you want to achieve. With identity-based habits, the focus is on who you wish to become.

* Decide the type of person you want to be. Prove it to yourself with small wins.

* Don't focus on goals, focus on the designing systems to achieve that goal.


## 7.Write about the book's perspective on how to make a good habit easier?

* A good habit can be cultivated in four stages :

* Cue.

* Craving.

* Response.

* Reward.


## 8.Write about the book's perspective on making a bad habit more difficult?

* Trying to achieve a habit that is harder.
* Trying to achieve a habit that less impact.

## 9.Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

* I will practice DSA daily.
    * I will subscribe to leetcode.

## 10.Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

* Less sleep.
    * I will put an alarm to remind met to sleep.
